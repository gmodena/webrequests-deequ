Experiments and EDA on webrequests using [Amazon Deequ](https://github.com/awslabs/deequ).

The following notebooks are available:
- `deequ.ipynb` tests integration and running things on Hadoop.
- `webrequests.python.EDA.ipynb`: EDA, metrics generation, repository, with pydeequ.
- `webrequests.scala.ipynb`: metrics generation, repository with scala api.

